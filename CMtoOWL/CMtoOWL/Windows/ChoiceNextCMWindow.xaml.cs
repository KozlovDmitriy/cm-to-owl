﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CMtoOWL.Windows
{
    /// <summary>
    /// Реализует окно выбора добавляемой КМ
    /// </summary>
    public partial class ChoiceNextCMWindow : Window
    {
        public CMType ChoiceCMType {
            get;
            private set;
        }

        public string Caption { 
            get; 
            private set; 
        }

        public ChoiceNextCMWindow( CMType type, bool hide_ccm = false ) {
            InitializeComponent();
            if ( type != CMType.CCM && type != CMType.MCM ) {
                lbi_ccm.Visibility = System.Windows.Visibility.Collapsed;
                lbi_mcm.Visibility = System.Windows.Visibility.Collapsed;
                lbi_gcm.Visibility = System.Windows.Visibility.Collapsed;
                lb_cm_types.SelectedIndex = 3;
            } else {
                lb_cm_types.SelectedIndex = 0;
            }
            if ( type == CMType.ACM ) {
                lbi_acm.Visibility = System.Windows.Visibility.Collapsed;
            }
            if ( type == CMType.CCM && hide_ccm ) {
                lbi_ccm.Visibility = System.Windows.Visibility.Collapsed;
                lb_cm_types.SelectedIndex = 1;
            }
            
        }

        private void btn_cancel_Click( object sender, RoutedEventArgs e ) {
            this.DialogResult = false;
            this.Close();
        }

        private void btn_ok_Click( object sender, RoutedEventArgs e ) {
            switch ( ( lb_cm_types.SelectedValue as ListBoxItem ).Content as string ) {                    
                case "Концептуальная КМ" : ChoiceCMType = CMType.CCM; break;
                case "Меронимическая КМ" : ChoiceCMType = CMType.MCM; break;
                case "Гиперанимическая КМ" : ChoiceCMType = CMType.GCM; break;
                case "Атрибутивная КМ" : ChoiceCMType = CMType.ACM; break;
                default: ChoiceCMType = CMType.NAN; break;
            }
            this.Caption = tb_caption.Text;
            this.DialogResult = true;
            this.Close();
        }

        private void lb_cm_types_SelectionChanged( object sender, SelectionChangedEventArgs e ) {
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            switch ( (lb_cm_types.SelectedValue as ListBoxItem).Content as string ) {
                case "Концептуальная КМ":
                    bitmap.UriSource = new Uri( @"/CMtoOWL;component/Resources/CMTypes/ККМ.png", UriKind.Relative );
                    break;
                case "Меронимическая КМ":
                    bitmap.UriSource = new Uri( @"/CMtoOWL;component/Resources/CMTypes/МКМ.png", UriKind.Relative );
                    break;
                case "Гиперанимическая КМ":
                    bitmap.UriSource = new Uri( @"/CMtoOWL;component/Resources/CMTypes/ГКМ.png", UriKind.Relative );
                    break;
                case "Атрибутивная КМ":
                    bitmap.UriSource = new Uri( @"/CMtoOWL;component/Resources/CMTypes/АКМ.png", UriKind.Relative );
                    break;
                default:
                    break;
            }
            bitmap.EndInit();
            image.Source = bitmap;
            tool_tip_image.Source = bitmap;
        }
    }
}
