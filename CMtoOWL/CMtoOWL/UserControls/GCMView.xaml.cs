﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.UserControls
{
    /// <summary>
    /// Реализует отображение Гиперонимической КМ.
    /// </summary>
    public partial class GCMView : HierarchicalCMView
    {
        #region Конструкторы

        public GCMView() {
            InitializeComponent();
            this.CMElements = new CMElementView[9] {
                element_1,
                element_2,
                element_3,
                element_4,
                element_5,
                element_6,
                element_7,
                element_8,
                element_9
            };
            this.CanvasWithCM = this.canvas_gcm;
        }        

        #endregion
    }
}
