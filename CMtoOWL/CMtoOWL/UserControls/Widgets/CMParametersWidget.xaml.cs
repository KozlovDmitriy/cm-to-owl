﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.UserControls.Widgets
{
    /// <summary>
    /// Реализует виджет отображающий основные параметры КМ.
    /// </summary>
    public partial class CMParametersWidget : UserControl
    {
        #region Делегаты

        public delegate void CMCaptionDelegat( string caption );
        public delegate void CMElementSelectionDelegat( string text );

        #endregion

        #region Статические поля

        public event CMCaptionDelegat CMCaptionChanged;
        public event CMElementSelectionDelegat CMElementSelectionChanged;

        #endregion

        #region Свойства

        public string CMCaption {
            get {
                return this.tb_cur_cm_name.Text;
            }
            set {
                this.tb_cur_cm_name.Text = value;
            }
        }

        public string SelectedCMElementText {
            get {
                return this.lb_cur_cm_elements.SelectedValue as string;
            }
            set {
                this.lb_cur_cm_elements.SelectedValue = value;
            }
        }

        public int SelectedCMElementIndex {
            get {
                return this.lb_cur_cm_elements.SelectedIndex;
            }
            set {
                this.lb_cur_cm_elements.SelectedIndex = value;
            }
        }

        #endregion

        #region Конструкторы

        public CMParametersWidget() {
            InitializeComponent();
        }

        #endregion

        #region События

        private void CMParametersWidget_Loaded( object sender, RoutedEventArgs e ) {
            this.lb_cur_cm_elements.Items.Clear();
        }

        private void tb_cur_cm_name_LostKeyboardFocus( object sender, KeyboardFocusChangedEventArgs e ) {
            this.CMCaptionChanged( this.CMCaption );
        }

        private void lb_cur_cm_elements_SelectionChanged( object sender, SelectionChangedEventArgs e ) {
            if ( this.lb_cur_cm_elements.SelectedIndex != -1 ) {
                this.CMElementSelectionChanged( this.lb_cur_cm_elements.SelectedValue as string );
            }
        }

        #endregion

        #region Методы

        public void UpdateCurrentCMElements( CM cm ) {
            if ( cm != null ) {
                this.lb_cur_cm_elements.Items.Clear();
                for ( int i = 0; i < 9; ++i ) {
                    if ( cm.CMElements[i].State == CMElementState.FULL || cm.CMElements[i].State == CMElementState.FULL_FIXED ) {
                        this.lb_cur_cm_elements.Items.Add( cm.CMElements[i].Text );
                    }
                }
            }
        }

        #endregion
    }
}
