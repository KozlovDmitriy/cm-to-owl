﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.UserControls.Widgets
{
    /// <summary>
    /// Реализует атрибут типа String
    /// </summary>
    public partial class StringListBoxAttributeItem : ListBoxItem
    {
        #region Поля

        private string attribute_name;

        #endregion

        #region Свойства

        public bool IsAttributeEnabled {
            get {
                return (bool) ( this.Template.FindName( "cb_enable", this ) as CheckBox ).IsChecked;
            }
            set {
                ( this.Template.FindName( "cb_enable", this ) as CheckBox ).IsChecked = value;
            }
        }

        public string AttributeName {
            get {
                return ( this.Template.FindName( "cb_enable", this ) as CheckBox ).Content as string;
            }
            set {
                ( this.Template.FindName( "cb_enable", this ) as CheckBox ).Content = value;
            }
        }

        public string Value {
            get {
                return ( this.Template.FindName( "value", this ) as TextBox ).Text;
            }
            set {
                ( this.Template.FindName( "value", this ) as TextBox ).Text = value;
            }
        }

        public GCMElement Element {
            get;
            private set;
        }

        public ACMElement Attribute {
            get;
            private set;
        }

        #endregion

        #region Конструкторы

        public StringListBoxAttributeItem( string attribute_name, GCMElement element, ACMElement attribute ) {
            InitializeComponent();
            this.attribute_name = attribute_name;
            this.Element = element;
            this.Attribute = attribute;
            this.Loaded += StringListBoxAttributeItem_Loaded;
        }

        #endregion

        #region События

        private void StringListBoxAttributeItem_Loaded( object sender, RoutedEventArgs e ) {
            this.AttributeName = this.attribute_name;
            object value = this.Element.GetValue( this.Attribute );
            if ( value != null ) {
                this.IsAttributeEnabled = true;
                this.Value = Convert.ToString( value ); 
            } else {
                this.IsAttributeEnabled = false;
            }
        }

        private void value_IsEnabledChanged( object sender, DependencyPropertyChangedEventArgs e ) {
            if ( CMElement.SelectedCMElement == this.Element ) {
                if ( !this.IsAttributeEnabled ) {
                    this.Element.SetValue( this.Attribute, null );
                }
            }
        }

        private void value_Changed( object sender, KeyboardFocusChangedEventArgs e ) {
            if ( this.Attribute.IsValueCorrect( this.Value ) ) {
                this.Element.SetValue( this.Attribute, this.Value );
            } else {
                MessageBox.Show( "Введенное значение не корректно." );
                this.Element.SetValue( this.Attribute, this.Attribute.GetDefaultValue() );
                this.Value = Convert.ToString( this.Attribute.GetDefaultValue() );
            }
        }

        #endregion
    }
}
