﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.UserControls.Widgets
{
    /// <summary>
    /// Реализует атрибут типа Boolean
    /// </summary>
    public partial class BooleanListBoxAttributeItem : ListBoxItem
    {
        #region Поля

        private string attribute_name;

        #endregion

        #region Свойства

        public bool IsAttributeEnabled {
            get {
                return (bool) ( this.Template.FindName("cb_enable", this) as CheckBox ).IsChecked;
            }
            set {
                ( this.Template.FindName( "cb_enable", this ) as CheckBox ).IsChecked = value;
            }
        }

        public string AttributeName {
            get {
                return ( this.Template.FindName( "value", this ) as CheckBox ).Content as string;
            }
            set {
                ( this.Template.FindName( "value", this ) as CheckBox ).Content = value;
            }
        }

        public bool Value {
            get {
                return (bool) ( this.Template.FindName( "value", this ) as CheckBox ).IsChecked; 
            }
            set {
                ( this.Template.FindName( "value", this ) as CheckBox ).IsChecked = value;
            }
        }

        public GCMElement Element {
            get;
            private set;
        }

        public ACMElement Attribute {
            get;
            private set;
        }

        #endregion

        #region Конструкторы

        public BooleanListBoxAttributeItem( string attribute_name, GCMElement element, ACMElement attribute ) {
            InitializeComponent();
            this.attribute_name = attribute_name;
            this.Element = element;
            this.Attribute = attribute;
            this.Loaded += BooleanListBoxAttributeItem_Loaded;
        }

        #endregion

        #region События

        private void BooleanListBoxAttributeItem_Loaded( object sender, RoutedEventArgs e ) {
            this.AttributeName = this.attribute_name;
            object value = this.Element.GetValue( this.Attribute );
            if ( value != null ) {
                this.IsAttributeEnabled = true;
                this.Value = Convert.ToBoolean( value );
            } else {
                this.IsAttributeEnabled = false;
            }
        }

        private void value_IsEnabledChanged( object sender, DependencyPropertyChangedEventArgs e ) {
            if ( !this.IsAttributeEnabled ) {
                this.Element.SetValue( this.Attribute, null );
            } else {
                this.Element.SetValue( this.Attribute, this.Value );
            }
        }

        private void value_Checked( object sender, RoutedEventArgs e ) {
            this.Element.SetValue( this.Attribute, this.Value );
        }

        #endregion
    }
}
