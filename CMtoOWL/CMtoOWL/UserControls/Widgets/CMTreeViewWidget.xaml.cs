﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.UserControls.Widgets
{
    /// <summary>
    /// Реализует виджет отображения дерева уровней КМ
    /// </summary>
    public partial class CMTreeViewWidget : UserControl
    {
        #region Делегаты

        public delegate void CMTreeViewDelegat( string header );

        #endregion

        #region Статические поля

        public event CMTreeViewDelegat CMTreeViewSelectionChanged;

        #endregion
        
        #region Конструкторы

        public CMTreeViewWidget() {
            InitializeComponent();
        }

        #endregion

        #region События

        private void OnItemMouseDoubleClick(object sender, MouseButtonEventArgs args) {
            if ( sender is Button ) {
                string header = ( ( sender as Button ).Content as ContentPresenter ).Content as string;
                this.CMTreeViewSelectionChanged( header );
            }
        }

        #endregion

        #region Методы

        public void RemoveSelections() {
            if ( this.cm_tree.SelectedItem != null ) {
                ( this.cm_tree.SelectedItem as TreeViewItem ).IsSelected = false;
            }
        }

        public void UpdateCMTreeView( CM cm ) {
            this.ClearTreeViewItems();
            TreeViewItem item = new TreeViewItem();
            item.Header = cm.Caption;
            this.cm_tree.Items.Add( item );
            this.AddCMTreeViewItemSubItems( cm, item );
        }

        public void ClearTreeViewItems() {
            this.cm_tree.Items.Clear();
        }

        #endregion

        #region Функции-члены

        private void AddCMTreeViewItemSubItems( CM cm, TreeViewItem item ) {
            foreach ( CMElement cm_element in cm.CMElements ) {
                foreach ( CMElement child in cm_element.Childrens ) {
                    if ( child != null ) {
                        TreeViewItem new_item = new TreeViewItem();
                        new_item.Header = child.CM.Caption;
                        item.Items.Add( new_item );
                        this.AddCMTreeViewItemSubItems( child.CM, new_item );
                    }
                }
            }
        }
        
        #endregion
    }
}
