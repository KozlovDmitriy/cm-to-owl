﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.UserControls.Widgets
{
    /// <summary>
    /// Виджет отображения созданных КМ
    /// </summary>
    public partial class CMSelectionTab : UserControl
    {
        #region Делегаты

        public delegate void CMSelectionTabDelegate( string cm_caption );
        public delegate void AddNewCMDelegat( CM cm );

        #endregion

        #region Поля

        public event CMSelectionTabDelegate CMSelectionTabSelectionChanged;
        public event AddNewCMDelegat AddNewCMToCMSelectionTab;

        #endregion

        #region Конструкторы

        public CMSelectionTab() {
            InitializeComponent();
        }

        #endregion

        #region Свойства

        public string SelectionCMHeader {
            get {
                if ( ( this.tc_cm_tab_control.SelectedItem as TabItem ) != null ) {
                    return ( this.tc_cm_tab_control.SelectedItem as TabItem ).Header as string;
                }
                return "";
            }
            set {
                if ( ( this.tc_cm_tab_control.SelectedItem as TabItem ) != null ) {
                    ( this.tc_cm_tab_control.SelectedItem as TabItem ).Header = value;
                }
            }
        }

        #endregion

        #region Методы

        public void ClearTabs() {
            this.tc_cm_tab_control.Items.Clear();
        }

        public void SelectCM( string header ) {
            foreach ( TabItem tab_item in this.tc_cm_tab_control.Items ) {
                if ( tab_item.Header as string == header ) {
                    tab_item.Visibility = System.Windows.Visibility.Visible;
                    tab_item.IsSelected = true;
                    break;
                }
            }
        }

        public CM AddNewCM( CMType type, CMElement cm_element, string name ) {
            CM cm = null;
            if ( type != CMType.NAN ) {
                TabItem lvl_tab_item = new TabItem();
                lvl_tab_item.Header = name;
                lvl_tab_item.IsSelected = true;
                switch ( type ) {
                    case CMType.ACM:
                        if ( cm_element == null || cm_element.Childrens[3] == null ) {
                            ACMView acm_view = new ACMView();
                            lvl_tab_item.Content = acm_view;
                            cm = new ACM( name, cm_element, acm_view );
                        } else {
                            this.SelectCM( cm_element.Childrens[3].CM.Caption );
                        }
                        break;
                    case CMType.CCM:
                        if ( cm_element == null || cm_element.Childrens[0] == null ) {
                            CCMView ccm_view = new CCMView();
                            lvl_tab_item.Content = ccm_view;
                            cm = new CCM( name, cm_element, ccm_view );
                        } else {
                            this.SelectCM( cm_element.Childrens[0].CM.Caption );
                        }
                        break;
                    case CMType.GCM:
                        if ( cm_element == null || cm_element.Childrens[2] == null ) {
                            GCMView gcm_view = new GCMView();
                            lvl_tab_item.Content = gcm_view;
                            cm = new GCM( name, cm_element, gcm_view );
                        } else {
                            this.SelectCM( cm_element.Childrens[2].CM.Caption );
                        }
                        break;
                    case CMType.MCM:
                        if ( cm_element == null || cm_element.Childrens[1] == null ) {
                            MCMView mcm_view = new MCMView();
                            lvl_tab_item.Content = mcm_view;
                            cm = new MCM( name, cm_element, mcm_view );
                        } else {
                            this.SelectCM( cm_element.Childrens[1].CM.Caption );
                        }
                        break;
                    default:
                        break;
                }
                if ( cm != null ) {
                    this.AddNewCMToCMSelectionTab( cm );
                    this.tc_cm_tab_control.Items.Add( lvl_tab_item );
                } else {
                    MessageBox.Show( "Выделенный элемент уже имеет потомка выбранного типа." );
                }
            }
            return cm;
        }

        #endregion

        #region События

        private void tc_cm_tab_control_SelectionChanged( object sender, SelectionChangedEventArgs e ) {
            Keyboard.ClearFocus();
            if ( ( this.tc_cm_tab_control.SelectedItem as TabItem ) != null ) {
                this.CMSelectionTabSelectionChanged( ( this.tc_cm_tab_control.SelectedItem as TabItem ).Header as string );
            }
        }

        private void btn_close_tab_Click( object sender, RoutedEventArgs e ) {
            if ( ( this.tc_cm_tab_control.SelectedItem as TabItem ) != null ) {
                DependencyObject obj = sender as DependencyObject;
                while ( ! (obj is TabItem) ) {
                    obj = VisualTreeHelper.GetParent( obj );
                }
                ( obj as TabItem ).Visibility = System.Windows.Visibility.Collapsed;
                this.tc_cm_tab_control.SelectedIndex = -1;
                foreach ( TabItem item in this.tc_cm_tab_control.Items ) {
                    if ( item.Visibility == System.Windows.Visibility.Visible ) {
                        item.IsSelected = true;
                    }
                }
            }
        }

        #endregion
    }
}
