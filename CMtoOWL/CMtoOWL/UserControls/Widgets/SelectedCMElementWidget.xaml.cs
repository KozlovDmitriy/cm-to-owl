﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMtoOWL.Model.ConfainmentModel;
using CMtoOWL.Enums;
using System.Windows.Threading;

namespace CMtoOWL.UserControls.Widgets
{
    /// <summary>
    /// реализует виджет отображения параметров выделенного элемента КМ
    /// </summary>
    public partial class SelectedCMElementWidget : UserControl
    {
        #region Делегаты

        public delegate void SelectedCMElementName( string name );
        public delegate void SelectedCMElementChidName( string name );

        #endregion

        #region Поля

        public event SelectedCMElementName SelectedCMElementNameChanged;
        public event SelectedCMElementChidName SelectionCMChanged;

        #endregion

        #region Конструкторы

        public SelectedCMElementWidget() {
            InitializeComponent();
            this.Loaded += SelectedCMElementWidget_Loaded;
        }

        #endregion

        #region Свойства

        public string CMElementName {
            get {
                return tb_cm_element_name.Text;
            }
            set {
                tb_cm_element_name.Text = value;
            }
        }

        #endregion

        #region Методы

        public void UpdateCMElementWidgetInfo( CMElement element ) {
            Keyboard.ClearFocus();
            //Название КМ к которой принадлежит элемент
            if ( element != null ) {
                this.tb_cur_element_cm_name.Text = element.CM.Caption;
            } else {
                this.tb_cur_element_cm_name.Text = ""; 
            }
            //Номер элемента            
            if ( element != null ) {
                this.tb_cur_element_number.Text = ( element.ElementNumber + 1 ).ToString();
            } else {
                this.tb_cur_element_number.Text = "";
            }
            //Имя элемента
            if ( element != null ) {
                this.tb_cm_element_name.Text = element.Text;
            } else {
                this.tb_cm_element_name.Text = "";
            }
            if ( element == null || element.State == CMElementState.FULL_FIXED ) {
                this.tb_cm_element_name.IsEnabled = false;
            } else {
                this.tb_cm_element_name.IsEnabled = true;
            }
            //Родитель элемента
            if ( element != null && element.Parent != null ) {
                this.btn_cur_element_parent.Content = element.Parent.CM.Caption;
                this.btn_cur_element_parent.IsEnabled = true;
            } else {
                this.btn_cur_element_parent.Content = "";
                this.btn_cur_element_parent.IsEnabled = false;
            }
            //Дети элемента
            this.lb_cur_element_childrens.Items.Clear();
            if ( element != null ) {
                for ( int i = 0; i < 4; ++i ) {
                    if ( element.Childrens[i] != null ) {
                        this.lb_cur_element_childrens.Items.Add( element.Childrens[i].CM.Caption );
                    }
                }
            }
            if ( this.lb_cur_element_childrens.Items.Count > 0 ) {
                this.lb_cur_element_childrens.IsEnabled = true;
            } else {
                this.lb_cur_element_childrens.IsEnabled = false;
            }
            //Тип атрибута
            if ( element != null && element is ACMElement && element.ElementNumber != 0 ) {
                this.cur_element_select_attr_type.Visibility = System.Windows.Visibility.Visible;
                this.cb_cur_element_select_attr_type.SelectedIndex = (int) ( element as ACMElement ).Type;
            } else {
                this.cur_element_select_attr_type.Visibility = System.Windows.Visibility.Hidden;
            }
            //Атрибуты
            List<ACMElement> attributes = null;
            if ( element != null ) {
                if ( !( element is ACMElement ) || element.ElementNumber == 0 ) {
                    attributes = element.GetAllElementAttributes();
                    this.lb_cur_element_attributes.Items.Clear();
                    this.lb_cur_element_attributes.IsEnabled = true;
                    if ( attributes.Count > 0 ) {
                        for ( int i = 0; i < attributes.Count; ++i ) {
                            if ( element is GCMElement && element.ElementNumber != 0 ) {
                                if ( attributes[i].Type.ToDescription() == "boolean" ) {
                                    this.lb_cur_element_attributes.Items.Add(
                                        new BooleanListBoxAttributeItem(
                                            attributes[i].Text,
                                            element as GCMElement,
                                            attributes[i]
                                        )
                                    );
                                } else {
                                    this.lb_cur_element_attributes.Items.Add(
                                        new StringListBoxAttributeItem(
                                            attributes[i].Text,
                                            element as GCMElement,
                                            attributes[i]
                                        )
                                    );
                                }
                            } else {
                                this.lb_cur_element_attributes.Items.Add( attributes[i].Text + " : " + attributes[i].Type.ToDescription() );
                            }
                        }
                    } else {
                        this.lb_cur_element_attributes.IsEnabled = false;
                    }
                } else {

                    this.lb_cur_element_attributes.IsEnabled = false;
                }
            } else {
                this.lb_cur_element_attributes.IsEnabled = false;
                this.lb_cur_element_attributes.Items.Clear();
            }
        }

        #endregion
        
        #region События

        private void SelectedCMElementWidget_Loaded( object sender, RoutedEventArgs e ) {
            this.lb_cur_element_attributes.Items.Clear();
        }

        private void lb_cur_element_attributes_SelectionChanged( object sender, SelectionChangedEventArgs e ) {
            if ( this.lb_cur_element_attributes.SelectedIndex != -1 && CMElement.SelectedCMElement is GCMElement ) {
                object value = ( CMElement.SelectedCMElement as GCMElement ).
                    GetValue( CMElement.SelectedCMElement.Attributes[this.lb_cur_element_attributes.SelectedIndex] );
            }
        }

        private void cb_cur_element_select_attr_type_SelectionChanged( object sender, SelectionChangedEventArgs e ) {
            if ( CMElement.SelectedCMElement is ACMElement && CMElement.SelectedCMElement.ElementNumber != 0 ) {
                ( CMElement.SelectedCMElement as ACMElement ).SetType( (this.cb_cur_element_select_attr_type.SelectedItem as ComboBoxItem).Content as string );
            }
        }

        private void tb_cm_element_name_LostKeyboardFocus( object sender, KeyboardFocusChangedEventArgs e ) {
            this.SelectedCMElementNameChanged( this.tb_cm_element_name.Text );
        }

        private void lb_cur_element_childrens_SelectionChanged( object sender, SelectionChangedEventArgs e ) {
            this.SelectionCMChanged( this.lb_cur_element_childrens.SelectedValue as string );
        }

        private void btn_cur_element_parent_Click( object sender, RoutedEventArgs e ) {
            this.SelectionCMChanged( this.btn_cur_element_parent.Content as string );
        }

        #endregion
    }
}
