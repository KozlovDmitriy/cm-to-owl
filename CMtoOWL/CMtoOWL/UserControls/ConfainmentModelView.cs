﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.UserControls
{
    /// <summary>
    /// Абстрактный класс отображения КМ.
    /// </summary>
    public abstract class ConfainmentModelView : UserControl
    {
        #region Поля

        protected SolidColorBrush[][] arrow_colors;

        #endregion

        #region Свойства

        public CMElementView[] CMElements {
            get;
            protected set;
        }

        public virtual SolidColorBrush[][] ArrowColors {
            get {
                return this.arrow_colors;
            }
        }

        public CM Model {
            get;
            private set;
        }

        public Canvas CanvasWithCM {
            get;
            protected set;
        }

        #endregion

        #region Методы

        public void AssociateWithModel( CM model ) {
            this.Model = model;
            for ( int i = 0; i < 9; ++i ) {
                this.CMElements[i].AssociateWithModel( model.CMElements[i] );
            }
        }

        /// <summary>
        /// Экспортирует отображение текущего уровня КМ в картинку.
        /// </summary>
        /// <param name="outStream">Выходной файловый поток.</param>
        public void ExportCanvasToImage( string filename, Line size_diag = null ) {
            FileStream out_stream = new FileStream( filename, FileMode.Create );
            Transform transform = this.CanvasWithCM.LayoutTransform;
            int scale = 4;
            this.CanvasWithCM.LayoutTransform = new ScaleTransform( scale, scale );
            Size size;
            if ( size_diag == null ) {
                size = new Size( this.CanvasWithCM.ActualWidth * scale, this.CanvasWithCM.ActualHeight * scale );
            } else {
                size = new Size(
                    Math.Abs( size_diag.X2 - size_diag.X1 ),
                    Math.Abs( size_diag.Y2 - size_diag.Y1 )
                );
            }
            this.CanvasWithCM.Measure( size );
            this.CanvasWithCM.Arrange( new Rect( size ) );
            this.CanvasWithCM.UpdateLayout();
            RenderTargetBitmap renderBitmap = new RenderTargetBitmap(
                (int) size.Width,
                (int) size.Height,
                96d,
                96d,
                PixelFormats.Pbgra32
            );
            renderBitmap.Render( this.CanvasWithCM );
            BitmapEncoder encoder;
            switch ( out_stream.Name.Substring( out_stream.Name.LastIndexOf( '.' ) ) ) {
                case "jpg":
                    encoder = new JpegBitmapEncoder();
                    break;
                case "png":
                    encoder = new PngBitmapEncoder();
                    break;
                case "gif":
                    encoder = new GifBitmapEncoder();
                    break;
                case "bmp":
                    encoder = new BmpBitmapEncoder();
                    break;
                default:
                    encoder = new TiffBitmapEncoder();
                    break;
            }
            encoder.Frames.Add( BitmapFrame.Create( renderBitmap ) );
            encoder.Save( out_stream );
            out_stream.Close();
            this.CanvasWithCM.LayoutTransform = transform;
        }

        #endregion
    }
}
