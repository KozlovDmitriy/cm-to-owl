﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;

namespace CMtoOWL.UserControls
{
    public abstract class HierarchicalCMView : ConfainmentModelView
    {
        private SolidColorBrush[][] arrow_colors = new SolidColorBrush[9][] { 
            new SolidColorBrush[9] { null, null, null, null, null, null, null, null, null  },
            new SolidColorBrush[9] { new SolidColorBrush( Brushes.YellowGreen.Color ), null, null, null, null, null, null, null, null  },
            new SolidColorBrush[9] { new SolidColorBrush( Brushes.YellowGreen.Color ), null, null, null, null, null, null, null, null  },
            new SolidColorBrush[9] { new SolidColorBrush( Brushes.YellowGreen.Color ), null, null, null, null, null, null, null, null  },
            new SolidColorBrush[9] { new SolidColorBrush( Brushes.YellowGreen.Color ), null, null, null, null, null, null, null, null  },
            new SolidColorBrush[9] { new SolidColorBrush( Brushes.YellowGreen.Color ), null, null, null, null, null, null, null, null  },
            new SolidColorBrush[9] { new SolidColorBrush( Brushes.YellowGreen.Color ), null, null, null, null, null, null, null, null  },
            new SolidColorBrush[9] { new SolidColorBrush( Brushes.YellowGreen.Color ), null, null, null, null, null, null, null, null  },
            new SolidColorBrush[9] { new SolidColorBrush( Brushes.YellowGreen.Color ), null, null, null, null, null, null, null, null  }
        };

        public override SolidColorBrush[][] ArrowColors {
            get {
                return this.arrow_colors;
            }
        }
    }
}
