﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.UserControls
{
    /// <summary>
    /// Реализует отображение Концептуальной КМ.
    /// </summary>
    public partial class CCMView : ConfainmentModelView
    {
        #region Поля

        private SolidColorBrush[][] arrow_colors = new SolidColorBrush[9][] { 
                                  // 1                                          2                                          3                                          4                                          5                                          6                                          7                                          8                                          9   
/*1*/       new SolidColorBrush[9] { null,                                      null,                                      null,                                      new SolidColorBrush( Brushes.Gray.Color ), null,                                      null,                                      new SolidColorBrush( Brushes.Gray.Color ), null,                                      new SolidColorBrush( Brushes.Gray.Color )  },
/*2*/       new SolidColorBrush[9] { new SolidColorBrush( Brushes.Gray.Color ), null,                                      new SolidColorBrush( Brushes.Gray.Color ), null,                                      new SolidColorBrush( Brushes.Gray.Color ), null,                                      null,                                      null,                                      null                                       },
/*3*/       new SolidColorBrush[9] { new SolidColorBrush( Brushes.Gray.Color ), null,                                      null,                                      new SolidColorBrush( Brushes.Gray.Color ), null,                                      null,                                      null,                                      null,                                      new SolidColorBrush( Brushes.Gray.Color )  },
/*4*/       new SolidColorBrush[9] { new SolidColorBrush( Brushes.Gray.Color ), null,                                      null,                                      null,                                      null,                                      null,                                      null,                                      new SolidColorBrush( Brushes.Gray.Color ), new SolidColorBrush( Brushes.Gray.Color )  },
/*5*/       new SolidColorBrush[9] { null,                                      new SolidColorBrush( Brushes.Gray.Color ), new SolidColorBrush( Brushes.Gray.Color ), new SolidColorBrush( Brushes.Gray.Color ), null,                                      null,                                      null,                                      null,                                      null                                       },
/*6*/       new SolidColorBrush[9] { null,                                      new SolidColorBrush( Brushes.Gray.Color ), new SolidColorBrush( Brushes.Gray.Color ), null,                                      new SolidColorBrush( Brushes.Gray.Color ), null,                                      null,                                      null,                                      null                                       },
/*7*/       new SolidColorBrush[9] { null,                                      new SolidColorBrush( Brushes.Gray.Color ), null,                                      null,                                      null,                                      new SolidColorBrush( Brushes.Gray.Color ), null,                                      new SolidColorBrush( Brushes.Gray.Color ), null                                       },
/*8*/       new SolidColorBrush[9] { null,                                      null,                                      null,                                      null,                                      new SolidColorBrush( Brushes.Gray.Color ), new SolidColorBrush( Brushes.Gray.Color ), new SolidColorBrush( Brushes.Gray.Color ), null,                                      null                                       },
/*9*/       new SolidColorBrush[9] { null,                                      null,                                      null,                                      null,                                      null,                                      new SolidColorBrush( Brushes.Gray.Color ), new SolidColorBrush( Brushes.Gray.Color ), new SolidColorBrush( Brushes.Gray.Color ), null                                       },
        };

        #endregion

        #region Конструкторы

        public CCMView() {
            InitializeComponent();
            this.CMElements = new CMElementView[9] {
                element_1,
                element_2,
                element_3,
                element_4,
                element_5,
                element_6,
                element_7,
                element_8,
                element_9
            };
            this.CanvasWithCM = this.canvas_ccm;
        }

        #endregion

        #region Свойства

        public override SolidColorBrush[][] ArrowColors {
            get {
                return this.arrow_colors;
            }
        }

        #endregion
    }
}
