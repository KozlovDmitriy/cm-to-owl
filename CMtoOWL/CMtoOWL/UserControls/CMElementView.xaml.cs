﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.UserControls
{
    /// <summary>
    /// Реализует отображение элемента КМ
    /// </summary>
    public partial class CMElementView : Button
    {
        #region Поля
        #endregion

        #region Свойства

        public InputTextElement TextElement {
            get;
            private set;
        }

        public string Text {
            get {
                return this.TextElement != null ? this.TextElement.Text : ( this.Model != null ? this.Model.Text : "" );
            }
            set {
                if ( this.TextElement != null ) {
                    this.TextElement.Text = value;
                }
            }
        }

        public CMElementState State {
            get;
            set;
        }

        public CMElement Model {
            get;
            private set;
        }

        #endregion

        #region Конструктуры

        public CMElementView() {
            InitializeComponent();
        }

        #endregion

        #region События

        private void cm_element_Click( object sender, RoutedEventArgs e ) {
            Keyboard.ClearFocus();
            this.Focus();
        }

        private void cm_element_MouseDoubleClick( object sender, MouseButtonEventArgs e ) {
            Keyboard.ClearFocus();
            if ( this.Model.State != CMElementState.FULL_FIXED ) {
                this.TextElement.KeyboardFocus();
            }
        }

        private void cm_element_Loaded( object sender, RoutedEventArgs e ) {
            this.TextElement = (InputTextElement) this.Template.FindName( "text_box_cm_element", this );
        }

        private void CMElementView_Loaded( object sender, RoutedEventArgs e ) {
            this.Text = this.Model.Text;
        }

        private void cm_element_MouseEnter( object sender, MouseEventArgs e ) {
            if ( this.Text == "" ) {
                ( this.Template.FindName( "tool_tip", this ) as ToolTip ).Visibility = System.Windows.Visibility.Hidden;
            } else {
                ( this.Template.FindName( "tool_tip_text", this ) as TextBlock ).Text = this.Text;
                ( this.Template.FindName( "tool_tip", this ) as ToolTip ).Visibility = System.Windows.Visibility.Visible;
            }
        }

        #endregion

        #region Методы

        public void AssociateWithModel( CMElement model ) {
            this.Model = model;
            this.Loaded += CMElementView_Loaded;
        }

        #endregion

    }
}
