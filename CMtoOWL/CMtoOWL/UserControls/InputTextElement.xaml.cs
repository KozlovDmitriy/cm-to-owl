﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CMtoOWL.UserControls
{
    /// <summary>
    /// Реализует блок ввода текста для элементов диаграммы.
    /// </summary>
    public partial class InputTextElement : TextBox
    {
        #region Конструкторы

        /// <summary>
        /// Конструктор по умолчанию.
        /// </summary>
        public InputTextElement() {
            InitializeComponent();
        }

        #endregion

        #region Методы

        /// <summary>
        /// Передает фокус полю ввода текста в <see cref="InternalDiagramElementTextBox"/>.
        /// </summary>
        public void KeyboardFocus() {
            this.Focusable = true;
            Dispatcher.BeginInvoke(
                DispatcherPriority.Input,
                new Action( delegate() {
                    this.Focus();           // Set Logical Focus
                    Keyboard.Focus( this ); // Set Keyboard Focus
                } )
            );
        }

        #endregion

        #region События

        private void tb_LostFocus( object sender, RoutedEventArgs e ) {
            Keyboard.ClearFocus();
            this.Focusable = false;
        }

        #endregion
    }
}
