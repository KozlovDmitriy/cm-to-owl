﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CMtoOWL.Enums
{
    /// <summary>
    /// Поддерживаемые типы атрибутов
    /// </summary>
    public enum AttributeType : uint
    {
        [DescriptionAttribute( "boolean" )]
        BOOLEAN_ATTRIBUTE_TYPE = 0,
        [DescriptionAttribute( "int" )]
        INT_ATTRIBUTE_TYPE = 1,
        [DescriptionAttribute( "float" )]
        FLOAT_ATTRIBUTE_TYPE = 2,
        [DescriptionAttribute( "string" )]
        STRING_ATTRIBUTE_TYPE = 3
    }

    static public class ExtensionMethods
    {
        public static string ToDescription( this Enum en ) {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember( en.ToString() );
            if ( memInfo != null && memInfo.Length > 0 ) {
                object[] attrs = memInfo[0].GetCustomAttributes( typeof( DescriptionAttribute ), false );
                if ( attrs != null && attrs.Length > 0 )
                    return ( (DescriptionAttribute) attrs[0] ).Description;
            }
            return en.ToString();
        }
    }
}
