﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMtoOWL
{
    public enum CMType : uint {
        CCM = 0,
        MCM = 1,
        GCM = 2,
        ACM = 3, 
        NAN = 4
    }
}
