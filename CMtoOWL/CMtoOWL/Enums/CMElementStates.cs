﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMtoOWL
{
    public enum CMElementState : uint
    {
        CANT_EDIT = 0,
        CAN_EDIT_EMPTY = 1,
        FULL = 2,
        FULL_FIXED = 3
    }
}
