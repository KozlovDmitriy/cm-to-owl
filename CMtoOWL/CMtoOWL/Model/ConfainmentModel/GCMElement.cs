﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMtoOWL.Model.ConfainmentModel
{
    /// <summary>
    /// Реализует элемент Гиперанимической КМ.
    /// </summary>
    public class GCMElement : CMElement
    {
        #region Свойства

        public ACMElement ACMChild {
            get {
                return base.Childrens[3] as ACMElement;
            }
            set {
                base.Childrens[3] = value;
                base.Parent = this;
            }
        }

        public Dictionary<ACMElement, object> AttributeWithValues {
            get;
            private set;
        }

        #endregion

        #region Конструкторы

        public GCMElement() {
            ACMElement.ACMElementTypeChanged += ACMElement_ACMElementTypeChanged;
            this.AttributeWithValues = new Dictionary<ACMElement, object>();
        }

        #endregion

        #region Методы

        public void CleanAttributeWithValues() {
            List<ACMElement> acm_elements = this.GetAllElementAttributes();
            foreach ( ACMElement key in this.AttributeWithValues.Keys ) {
                if ( !acm_elements.Contains( key ) ) {
                    this.AttributeWithValues.Remove( key );
                }
            }
        }

        public bool SetValue( ACMElement attribute, object value ) {
            bool fl;
            if ( attribute.IsValueCorrect( value ) ) {
                if ( !this.AttributeWithValues.ContainsKey( attribute ) ) {
                    this.AttributeWithValues.Add( attribute, value );
                } else {
                    this.AttributeWithValues[attribute] = value;
                }
                fl = true;
            } else {
                if ( this.AttributeWithValues.ContainsKey( attribute ) ) {
                    this.AttributeWithValues[attribute] = attribute.GetDefaultValue();
                }
                fl = false;
            }
            return fl;
        }

        public object GetValue( ACMElement attribute ) {
            if ( this.AttributeWithValues.ContainsKey( attribute ) ) {
                return this.AttributeWithValues[attribute];
            }
            return null;
        }

        public override string SerializeData() {
            string data = base.SerializeData();
            foreach ( KeyValuePair<ACMElement, object> item in this.AttributeWithValues ) {
                data += "Attribute Name : " + item.Key.Text + "\r\n";
                data += "Attribute Value : " + item.Value.ToString() + "\r\n";
            }
            return data;
        }

        #endregion

        #region События

        private void ACMElement_ACMElementTypeChanged( ACMElement acm_element ) {
            if ( this.AttributeWithValues.ContainsKey( acm_element ) ) {
                this.AttributeWithValues[ acm_element ] = acm_element.GetDefaultValue();
            }
        }

        #endregion
    }
}
