﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Model.OwlTree;
using CMtoOWL.UserControls;

namespace CMtoOWL.Model.ConfainmentModel
{
    /// <summary>
    /// Реализует Атрибутивную КМ
    /// </summary>
    public class ACM : CM
    {
        #region Конструкторы

        public ACM() {
            InitializeACM();
        }

        public ACM( string caption, CMElement main_element, ACMView view ) {
            this.InitializeACM();
            this.View = view;
            if ( main_element != null ) {
                this.ConnectToUpLevel( main_element );
            }
            this.AssociateWithView( view );
            this.Caption = caption;
            view.Loaded += view_Loaded;
        }       

        public ACM( ACMView view ) {
            InitializeACM();
            this.View = view;
            this.AssociateWithView( view );
            view.Loaded += view_Loaded;
        }

        #endregion

        #region Методы

        public void AssociateWithView( ACMView view ) {
            for ( int i = 0; i < 9; ++i ) {
                this.CMElements[i].View = view.CMElements[i];
            }
        }

        #endregion

        #region Функции-члены

        private void InitializeACM() {
            this.CMElements = new ACMElement[9];
            for ( int i = 0; i < 9; ++i ) {
                this.CMElements[i] = new ACMElement();
                this.CMElements[i].CM = this;
                this.CMElements[i].Text = "";
            }
        }

        private void ConnectToUpLevel( CMElement main_element ) {
            main_element.Childrens[3] = this.CMElements[0];
            this.CMElements[0].Parent = main_element;
            this.CMElements[0].Text = main_element.Text;
        }

        #endregion

        #region События

        private void view_Loaded( object sender, System.Windows.RoutedEventArgs e ) {
            View.AssociateWithModel( this );
            this.CMElements[0].State = CMElementState.FULL_FIXED;
        }

        #endregion

        #region Переопределенные методы

        public override void ToOwlTree( OwlTreeNode main_node ) {
            for ( int i = 1; i < 9; ++i ) {
                if ( this.CMElements[i].State == CMElementState.FULL ) {
                    OwlTreeNodeAttribute node = new OwlTreeNodeAttribute(
                        this.CMElements[i].Text, 
                        ( this.CMElements[i] as ACMElement ).Type, 
                        this.CMElements[i] as ACMElement, 
                        main_node 
                    );
                }
            }
        }

        #endregion
    }
}
