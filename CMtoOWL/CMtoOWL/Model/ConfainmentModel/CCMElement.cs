﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMtoOWL.Model.ConfainmentModel
{
    /// <summary>
    /// Реализует элемент Концептуальной КМ.
    /// </summary>
    public class CCMElement : CMElement
    {
        #region Свойства

        public CCMElement CCMChild {
            get {
                return base.Childrens[0] as CCMElement;
            }
            set {
                base.Childrens[0] = value;
                base.Parent = this;
            }
        }

        public GCMElement GCMChild {
            get {
                return base.Childrens[1] as GCMElement;
            }
            set {
                base.Childrens[1] = value;
                base.Parent = this;
            } 
        }

        public MCMElement MCMChild {
            get {
                return base.Childrens[2] as MCMElement;
            }
            set {
                base.Childrens[2] = value;
                base.Parent = this;
            }
        }

        public ACMElement ACMChild {
            get {
                return base.Childrens[3] as ACMElement;
            }
            set {
                base.Childrens[3] = value;
                base.Parent = this;
            }
        }

        #endregion

        #region Конструкторы

        public CCMElement() { }

        #endregion
    }
}
