﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Model.OwlTree;
using CMtoOWL.UserControls;

namespace CMtoOWL.Model.ConfainmentModel
{
    /// <summary>
    /// Реализует Концептуальную КМ
    /// </summary>
    public class CCM : CM
    {
        #region Функции-члены

        private void InitializeCCM() {
            this.CMElements = new CCMElement[9];
            for ( int i = 0; i < 9; ++i ) {
                this.CMElements[i] = new CCMElement();
                this.CMElements[i].CM = this;
                this.CMElements[i].Text = "";
            }
        }

        protected void ConnectToUpLevel( CMElement main_element ) {
            main_element.Childrens[0] = this.CMElements[0];
            this.CMElements[0].Parent = main_element;
            this.CMElements[0].Text = main_element.Text;
        }
        
        #endregion

        #region Конструкторы

        public CCM() {
            InitializeCCM();
        }

        public CCM( string caption, CMElement main_element, CCMView view ) {
            this.InitializeCCM();
            if ( main_element != null ) {
                this.ConnectToUpLevel( main_element );
            }
            this.View = view;
            this.Caption = caption;
            view.Loaded += view_Loaded;
        }  

        public CCM( CCMView view ) {
            InitializeCCM();
            this.View = view;
            this.AssociateWithView( view );
            view.Loaded += view_Loaded;
            this.CMElements[0].State = CMElementState.CAN_EDIT_EMPTY;
        }

        #endregion

        #region События

        private void view_Loaded( object sender, System.Windows.RoutedEventArgs e ) {
            View.AssociateWithModel( this );
            if ( this.CMElements[0].Text != "" ) {
                if ( this.CMElements[0].Parent != null ) {
                    this.CMElements[0].State = CMElementState.FULL_FIXED;
                } else {
                    this.CMElements[0].State = CMElementState.FULL;
                }
            } else {
                this.CMElements[0].State = CMElementState.CAN_EDIT_EMPTY;
            }
            this.CMElements[0].UpdateCmElementsState();
        }

        #endregion

        #region Переопределенные методы

        public override void ToOwlTree( OwlTreeNode main_node ) {
            if ( this.CMElements[0].State != CMElementState.FULL_FIXED ) {
                OwlClassNode node = new OwlClassNode( this.CMElements[0].Text, this.CMElements[0], main_node as OwlClassNode );
                for ( int k = 0; k < 4; ++k ) {
                    if ( this.CMElements[0].Childrens[k] != null ) {
                        this.CMElements[0].Childrens[k].CM.ToOwlTree( node );
                    }
                }
            }
            for ( int i = 1; i < 9; ++i ) {
                if ( this.CMElements[i].State == CMElementState.FULL ) {
                    OwlClassNode node = null;
                    if ( this.CMElements[0].State == CMElementState.FULL_FIXED ) {
                        node = new OwlClassNode( this.CMElements[i].Text, this.CMElements[i], main_node.Parent );
                    } else {
                        node = new OwlClassNode( this.CMElements[i].Text, this.CMElements[i], main_node as OwlClassNode );
                    }
                    for ( int k = 0; k < 4; ++k ) {
                        if ( this.CMElements[i].Childrens[k] != null ) {
                            this.CMElements[i].Childrens[k].CM.ToOwlTree( node );
                        }
                    }
                }
            }
        }

        #endregion
    }
}
