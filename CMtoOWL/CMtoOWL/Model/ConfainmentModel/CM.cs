﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Model.MathModel;
using CMtoOWL.Model.OwlTree;
using CMtoOWL.UserControls;

namespace CMtoOWL.Model.ConfainmentModel
{
    /// <summary>
    /// Реализует абстрактный класс КМ
    /// </summary>
    public abstract class CM
    {
        #region Поля

        private ConfainmentModelView view;

        #endregion

        #region Свойства

        public ConfainmentModelView View {
            get { return view; }
            set {
                this.view = value;
                this.AssociateWithView( this.view );
            }
        }
        public string Caption {
            get;
            set;
        }

        public CMElement[] CMElements {
            get;
            set;
        }

        #endregion

        #region Методы

        public string ToOwl() {
            OwlClassNode main_owl_tree_node = new OwlClassNode( "Thing", null, null );
            this.ToOwlTree( main_owl_tree_node );
            string owl_script = OwlScript.GetHeader();
            foreach ( OwlTreeNode node in main_owl_tree_node.Childrens ) {
                owl_script += node.GetNamedDeclarations();
            }
            foreach ( OwlTreeNode node in main_owl_tree_node.Childrens ) {
                owl_script += node.GetNodeRelationDeclarations();
            }
            foreach ( OwlTreeNode node in main_owl_tree_node.Childrens ) {
                owl_script += node.GetIndividualsAttributes();
            }
            owl_script += OwlScript.GetFooter();
            return owl_script;
        }

        public abstract void ToOwlTree( OwlTreeNode main_node );

        public void AssociateWithView( ConfainmentModelView view ) {
            for ( int i = 0; i < 9; ++i ) {
                this.CMElements[i].View = view.CMElements[i];
            }
            this.CMElements[0].UpdateCmElementsState();
            if ( CMElements[0].State == CMElementState.CANT_EDIT ) {
                CMElements[0].State = CMElementState.CAN_EDIT_EMPTY;
            }
        }        

        public string SerializeData() {
            string data = "";
            data += "CM : " + this.GetType().Name.ToString() + "\r\n";
            data += "Caption : " + this.Caption + "\r\n";
            for ( int i = 0; i < 9; ++i ) {
                if ( this.CMElements[i].State == CMElementState.FULL ||
                    this.CMElements[i].State == CMElementState.FULL_FIXED ) {
                    data += this.CMElements[i].SerializeData();
                }
            }
            data += "\r\n";
            for ( int i = 1; i < 9; ++i ) {
                for ( int k = 3; k >= 0; --k ) {
                    if ( this.CMElements[i].Childrens[k] != null && this.CMElements[i].State == CMElementState.FULL ) {
                        data += this.CMElements[i].Childrens[k].CM.SerializeData();
                    }
                }
            }
            return data;
        }

        #endregion
    }
}
