﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using CMtoOWL.Enums;

namespace CMtoOWL.Model.ConfainmentModel
{
    /// <summary>
    /// Реализует элемент Атрибутивной КМ.
    /// </summary>
    public class ACMElement : CMElement
    {
        #region Объявления события

        public delegate void ACMElementTypeDelegate( ACMElement acm_element );
        public static event ACMElementTypeDelegate ACMElementTypeChanged;

        #endregion

        #region Свойства

        public AttributeType Type {
            get;
            set;
        }

        #endregion

        #region Конструкторы

        public ACMElement() {
            this.Type = AttributeType.BOOLEAN_ATTRIBUTE_TYPE;
        }

        #endregion

        #region Методы

        public bool IsValueCorrect( object value ) {
            switch ( Type.ToDescription() ) {
                case "boolean":
                    try {
                        Convert.ToBoolean( value );
                        return true;
                    }
                    catch {
                        return false;
                    }
                case "int":
                    try {
                        Convert.ToInt32( value );
                        return true;
                    }
                    catch {
                        return false;
                    }
                case "float":
                    try {
                        Convert.ToDouble( value );
                        return true;
                    }
                    catch {
                        return false;
                    }
                case "string":
                    try {
                        Convert.ToString( value );
                        return true;
                    }
                    catch {
                        return false;
                    }
                default: return false;
            }
        }

        public object GetDefaultValue() {
            switch ( Type.ToDescription() ) {
                case "boolean":
                    return false;
                case "int":
                    return 0;
                case "float":
                    return 0.0;
                case "string":
                    return "";
                default: return null;
            }
        }

        public void SetType( string acm_type ) {
            if ( this.Type.ToDescription() != acm_type ) {
                switch ( acm_type ) {
                    case "boolean":
                        this.Type = Enums.AttributeType.BOOLEAN_ATTRIBUTE_TYPE;
                        break;
                    case "int":
                        this.Type = Enums.AttributeType.INT_ATTRIBUTE_TYPE;
                        break;
                    case "float":
                        this.Type = Enums.AttributeType.FLOAT_ATTRIBUTE_TYPE;
                        break;
                    case "string":
                        this.Type = Enums.AttributeType.STRING_ATTRIBUTE_TYPE;
                        break;
                }
                if ( ACMElement.ACMElementTypeChanged != null ) {
                    ACMElement.ACMElementTypeChanged( this );
                }
            }
        }

        public override string SerializeData() {
            string data = base.SerializeData();
            data += "Type : " + this.Type.ToDescription() + "\r\n";
            return data;
        }

        #endregion
    }
}
