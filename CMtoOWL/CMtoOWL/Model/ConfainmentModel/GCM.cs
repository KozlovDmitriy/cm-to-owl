﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Model.OwlTree;
using CMtoOWL.UserControls;

namespace CMtoOWL.Model.ConfainmentModel
{
    /// <summary>
    /// Реализует Гиперанимическую КМ
    /// </summary>
    public class GCM : CM
    {
        #region Функции-члены

        private void InitializeGCM() {
            this.CMElements = new GCMElement[9];
            for ( int i = 0; i < 9; ++i ) {
                this.CMElements[i] = new GCMElement();
                this.CMElements[i].CM = this;
                this.CMElements[i].Text = "";
            }
        }

        protected void ConnectToUpLevel( CMElement main_element ) {
            main_element.Childrens[2] = this.CMElements[0];
            this.CMElements[0].Parent = main_element;
            this.CMElements[0].Text = main_element.Text;
        }

        #endregion

        #region Конструкторы

        public GCM() {
            InitializeGCM();
        }

        public GCM( string caption, CMElement main_element, GCMView view ) {
            this.InitializeGCM();
            this.View = view;
            if ( main_element != null ) {
                this.ConnectToUpLevel( main_element );
            }
            this.AssociateWithView( view );
            this.Caption = caption;
            view.Loaded += view_Loaded;
        }  

        public GCM( GCMView view ) {
            InitializeGCM();
            this.View = view;
            this.AssociateWithView( view );
            view.Loaded += view_Loaded;
        }

        #endregion

        #region Методы

        public void AssociateWithView( GCMView view ) {
            for ( int i = 0; i < 9; ++i ) {
                this.CMElements[i].View = view.CMElements[i];
            }
        }

        #endregion

        #region События

        private void view_Loaded( object sender, System.Windows.RoutedEventArgs e ) {
            View.AssociateWithModel( this );
            this.CMElements[0].State = CMElementState.FULL_FIXED;
        }

        #endregion

        #region Переопределенные методы

        public override void ToOwlTree( OwlTreeNode main_node ) {
            for ( int i = 1; i < 9; ++i ) {
                if ( this.CMElements[i].State == CMElementState.FULL ) {
                    OwlIndividualNode node = new OwlIndividualNode( this.CMElements[i].Text, this.CMElements[i], main_node as OwlClassNode );
                    for ( int k = 0; k < 4; ++k ) {
                        if ( this.CMElements[i].Childrens[k] != null ) {
                            this.CMElements[i].Childrens[k].CM.ToOwlTree( node );
                        }
                    }
                }
            }
        }

        #endregion
    }
}
