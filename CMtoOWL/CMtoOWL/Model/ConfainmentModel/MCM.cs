﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Model.OwlTree;
using CMtoOWL.UserControls;

namespace CMtoOWL.Model.ConfainmentModel
{
    /// <summary>
    /// Реализует Меронимическую КМ
    /// </summary>
    public class MCM : CM
    { 
        #region Конструкторы

        public MCM() {
            InitializeMCM();
        }        

        public MCM( string caption, CMElement main_element, MCMView view ) {
            this.InitializeMCM();
            this.View = view;
            if ( main_element != null ) {
                this.ConnectToUpLevel( main_element );
            }
            this.AssociateWithView( view );
            this.Caption = caption;
            view.Loaded += view_Loaded;
        }  

        public MCM( MCMView view ) {
            InitializeMCM();
            this.View = view;
            this.AssociateWithView( view );
            view.Loaded += view_Loaded;
        }

        #endregion

        #region Функции-члены

        protected void ConnectToUpLevel( CMElement main_element ) {
            main_element.Childrens[1] = this.CMElements[0];
            this.CMElements[0].Parent = main_element;
            this.CMElements[0].Text = main_element.Text;
        }

        private void InitializeMCM() {
            this.CMElements = new MCMElement[9];
            for ( int i = 0; i < 9; ++i ) {
                this.CMElements[i] = new MCMElement();
                this.CMElements[i].CM = this;
                this.CMElements[i].Text = "";
            }
        }

        #endregion

        #region События

        private void view_Loaded( object sender, System.Windows.RoutedEventArgs e ) {
            this.View.AssociateWithModel( this );
            this.CMElements[0].State = CMElementState.FULL_FIXED;
        }

        #endregion

        #region Переопределенные методы

        public override void ToOwlTree( OwlTreeNode main_node ) {
            for ( int i = 1; i < 9; ++i ) {
                if ( this.CMElements[i].State == CMElementState.FULL ) {
                    OwlClassNode node = new OwlClassNode( this.CMElements[i].Text, this.CMElements[i], main_node as OwlClassNode );
                    for ( int k = 0; k < 4; ++k ) {
                        if ( this.CMElements[i].Childrens[k] != null ) {
                            this.CMElements[i].Childrens[k].CM.ToOwlTree( node );
                        }
                    }
                }
            }
        }

        #endregion
    }
}
