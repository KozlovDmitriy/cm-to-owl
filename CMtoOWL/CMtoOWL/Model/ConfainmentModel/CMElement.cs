﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using System.Windows.Media;
using CMtoOWL.UserControls;

namespace CMtoOWL.Model.ConfainmentModel
{    
    /// <summary>
    /// Реализует элемент КМ 
    /// </summary>
    public abstract class CMElement
    {
        #region Делегаты

        public delegate void CMElementDelegate ();

        #endregion

        #region Статические поля

        public static event CMElementDelegate CMElementTextChanged;
        public static event CMElementDelegate CMElementSelectChanged;

        #endregion

        #region Поля

        private CMElementView view;
        private bool is_fixed = false;
        private string text;

        #endregion

        #region Свойства

        public static CMElement SelectedCMElement {
            get;
            private set;
        }

        public string Text {
            get {
                return this.text;
            }
            set {
                if ( this.text != value ) {
                    this.OldText = this.Text;
                    if ( value != "" ) {
                        this.State = CMElementState.FULL;
                    } else {
                        this.State = CMElementState.CAN_EDIT_EMPTY;
                    }
                    this.text = value;
                    foreach ( CMElement child in this.Childrens ) {
                        if ( child != null ) {
                            child.Text = text;
                        }
                    }
                    if ( this.View != null && this.View.Text != value ) {
                        this.View.Text = value;
                    }
                    if ( this.CM != null ) {
                        if ( this.Parent == null ) {
                            CMElement old = CMElement.SelectedCMElement;
                            CMElement.SelectedCMElement = this;
                            if ( this.IsEnableEvents ) {
                                CMElement.CMElementTextChanged();
                            }
                            CMElement.SelectedCMElement = old;
                        }
                    }
                    this.UpdateCmElementsState();
                }
            }
        }

        /// <summary>
        /// Текст элемента перед редактированием
        /// </summary>
        public string OldText {
            get;
            private set;
        }

        public CMElement Parent {
            get;
            set;
        }

        public CMElement[] Childrens {
            get;
            protected set;
        }

        public CM CM {
            get;
            set;
        }

        public CMElementView View {
            get { return this.view; }
            set {
                this.view = value;
                this.AssociateWithView();
            }
        }

        public CMElementState OldState {
            get;
            private set;
        }

        public CMElementState State {
            get {
                if ( this.View != null ) {
                    if ( this.View.BorderBrush == Brushes.YellowGreen ) {
                        return CMElementState.CAN_EDIT_EMPTY;
                    }
                    if ( this.View.BorderBrush == Brushes.DarkRed ) {
                        return is_fixed ? CMElementState.FULL_FIXED : CMElementState.FULL;
                    }
                }
                return CMElementState.CANT_EDIT;
            }
            set {
                if ( this.View != null ) {
                    this.View.State = value;
                    this.OldState = this.State;
                    if ( value == CMElementState.CANT_EDIT ) {
                        this.View.BorderBrush = Brushes.Black;
                        this.View.IsEnabled = false;
                    }
                    if ( value == CMElementState.CAN_EDIT_EMPTY ) {
                        this.View.BorderBrush = Brushes.YellowGreen;
                        this.View.IsEnabled = true;
                    }
                    if ( value == CMElementState.FULL ) {
                        this.View.BorderBrush = Brushes.DarkRed;
                        this.View.IsEnabled = true;
                    }
                    if ( value == CMElementState.FULL_FIXED ) {
                        this.View.BorderBrush = Brushes.DarkRed;
                        this.View.IsEnabled = true;
                        this.is_fixed = true;
                    } else {
                        this.is_fixed = false;
                    }
                }
            }
        }

        public bool IsEnableEvents {
            get;
            set;
        }

        public int ElementNumber {
            get {
                if ( this.CM != null ) {
                    for ( int i = 0; i < 9; ++i ) {
                        if ( this.CM.CMElements[i] == this ) {
                            return i;
                        }
                    }
                }
                return -1;
            }
        }

        public List<ACMElement> Attributes {
            get;
            set;
        }

        #endregion

        #region Конструкторы

        public CMElement() {
            this.Childrens = new CMElement[4] { null, null, null, null };
            this.IsEnableEvents = true;
            this.Text = "";
        }

        #endregion

        #region Функции-члены

        private void AssociateWithView() {
            if ( view.TextElement != null ) {
                view.TextElement.LostKeyboardFocus += TextElement_LostKeyboardFocus;
                view.Click += view_Click;
                view.LostFocus += view_LostFocus;   
            } else { 
                view.Loaded += view_Loaded;
            }
        }

        private void UpdateCMElementState( int i ) {
            if ( this.CM != null ) {
                if ( this.CM.CMElements[i].Text == "" ) {
                    this.CM.CMElements[i].State = CMElementState.CAN_EDIT_EMPTY;
                } else {
                    this.CM.CMElements[i].State = CMElementState.FULL;
                }
            }
        }

        private void UpdateCMArrowStates() {
            if ( this.CM != null && this.CM.View != null) {
                for ( int i = 0; i < this.CM.View.ArrowColors.Length; ++i ) {
                    for ( int k = 0; k < this.CM.View.ArrowColors[i].Length; ++k ) {
                        if ( this.CM.View.ArrowColors[i][k] != null ) {
                            this.CM.View.ArrowColors[i][k].Color = Brushes.DarkRed.Color;
                            if ( this.CM.CMElements[i].State == CMElementState.CAN_EDIT_EMPTY ||
                                this.CM.CMElements[k].State == CMElementState.CAN_EDIT_EMPTY ) {
                                this.CM.View.ArrowColors[i][k].Color = Brushes.YellowGreen.Color;
                            }
                            if ( this.CM.CMElements[i].State == CMElementState.CANT_EDIT ||
                                this.CM.CMElements[k].State == CMElementState.CANT_EDIT ) {
                                this.CM.View.ArrowColors[i][k].Color = Brushes.Gray.Color;
                            }
                        }
                    }
                }
            }
        }

        public void UpdateCmElementsState() {
            if ( this.CM is CCM ) {
                if ( this.CM.CMElements[0].State == CMElementState.FULL ||
                    this.CM.CMElements[0].State == CMElementState.FULL_FIXED  ) {
                    if ( this.CM.CMElements[1].State != CMElementState.FULL ) {
                        this.UpdateCMElementState( 1 );
                    }
                    if ( this.CM.CMElements[2].State != CMElementState.FULL ) {
                        this.UpdateCMElementState( 2 );
                    }
                    if ( this.CM.CMElements[3].State != CMElementState.FULL ) {
                        this.UpdateCMElementState( 3 );
                    }
                } else {
                    this.CM.CMElements[1].State = CMElementState.CANT_EDIT;
                    this.CM.CMElements[2].State = CMElementState.CANT_EDIT;
                    this.CM.CMElements[3].State = CMElementState.CANT_EDIT;
                }
                if ( this.CM.CMElements[1].State == CMElementState.FULL &&
                    this.CM.CMElements[2].State == CMElementState.FULL &&
                    this.CM.CMElements[3].State == CMElementState.FULL ) {
                        if ( this.CM.CMElements[4].State != CMElementState.FULL ) {
                            this.UpdateCMElementState( 4 );
                    }
                } else {
                    this.CM.CMElements[4].State = CMElementState.CANT_EDIT;
                }
                if ( this.CM.CMElements[4].State == CMElementState.FULL ) {
                    if ( this.CM.CMElements[5].State != CMElementState.FULL ) {
                        this.UpdateCMElementState( 5 );
                    }
                } else {
                    this.CM.CMElements[5].State = CMElementState.CANT_EDIT;
                }
                if ( this.CM.CMElements[5].State == CMElementState.FULL ) {
                    if ( this.CM.CMElements[6].State != CMElementState.FULL ) {
                        this.UpdateCMElementState( 6 );
                    }
                } else {
                    this.CM.CMElements[6].State = CMElementState.CANT_EDIT;
                }
                if ( this.CM.CMElements[6].State == CMElementState.FULL ) {
                    if ( this.CM.CMElements[7].State != CMElementState.FULL ) {
                        this.UpdateCMElementState( 7 );
                    }
                } else {
                    this.CM.CMElements[7].State = CMElementState.CANT_EDIT;
                }
                if ( this.CM.CMElements[7].State == CMElementState.FULL ) {
                    if ( this.CM.CMElements[8].State != CMElementState.FULL ) {
                        this.UpdateCMElementState( 8 );
                    }
                } else {
                    this.CM.CMElements[8].State = CMElementState.CANT_EDIT;
                }
            }
            if ( this.CM is ACM || this.CM is GCM || this.CM is MCM ) {
                if ( this.CM.CMElements[0].Text == "" ) {
                    for ( int i = 0; i < 9; ++i ) {
                        this.CM.CMElements[i].State = CMElementState.CANT_EDIT;
                    }
                } else {
                    this.CM.CMElements[0].State = CMElementState.FULL_FIXED;
                    for ( int i = 1; i < 9; ++i ) {
                        this.UpdateCMElementState( i );
                    }
                }
            }
            this.UpdateCMArrowStates();
        }

        #endregion

        #region Методы

        public void Select() {
            this.view_Click( null, null ); 
            Keyboard.ClearFocus();
            this.View.Focus();
        }

        public List<ACMElement> GetAllElementAttributes() {
            List<ACMElement> attributes = new List<ACMElement>();
            if ( !( this is ACMElement ) ) {
                if ( this.Childrens[3] != null ) {
                    for ( int i = 1; i < 9; ++i ) {
                        if ( this.Childrens[3].CM.CMElements[i].State == CMElementState.FULL ) {
                            attributes.Add( this.Childrens[3].CM.CMElements[i] as ACMElement );
                        }
                    }
                }
                if ( this is MCMElement || this is GCMElement ) {
                    attributes.AddRange( this.CM.CMElements[0].Parent.GetAllElementAttributes() );
                } else {
                    if ( this is CCMElement ) {
                        CMElement element = this.CM.CMElements[0];
                        while ( element.Parent != null && !( element.Parent is MCMElement ) ) {
                            element = element.Parent.CM.CMElements[0];
                        }
                        if ( element is MCMElement ) {
                            attributes.AddRange( element.CM.CMElements[0].Parent.GetAllElementAttributes() );
                        }
                    }
                }
            } else {
                if ( this.ElementNumber == 0 ) {
                    attributes.AddRange( this.Parent.GetAllElementAttributes() );
                }
            }
            this.Attributes = attributes;
            return attributes;
        }

        public virtual string SerializeData() {
            string data = "";
            data += "Element Number : " + this.ElementNumber.ToString() + "\r\n";
            data += "Element Text : " + this.Text + "\r\n";
            return data;
        }

        #endregion

        #region События

        private void view_Click( object sender, System.Windows.RoutedEventArgs e ) {
            if ( CMElement.SelectedCMElement != this ) {
                CMElement.SelectedCMElement = this;
                CMElement.CMElementSelectChanged();
            }
        }

        private void view_LostFocus( object sender, System.Windows.RoutedEventArgs e ) {
            if ( CMElement.SelectedCMElement == this ) {
                CMElement.SelectedCMElement = null;
                if ( this.IsEnableEvents ) {
                    CMElement.CMElementSelectChanged();
                }
            }
        }

        private void TextElement_LostKeyboardFocus( object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e ) {
            this.Text = this.View.Text;
        }

        private void view_Loaded( object sender, System.Windows.RoutedEventArgs e ) {
            AssociateWithView();
        }

        #endregion

        #region Статические методы

        public static CMType GetCurrentCMType() {
            if ( CMElement.SelectedCMElement == null ) {
                return CMType.NAN;
            } else if ( CMElement.SelectedCMElement is CCMElement ) {
                return CMType.CCM;
            } else if ( CMElement.SelectedCMElement is GCMElement ) {
                return CMType.GCM;
            } else if ( CMElement.SelectedCMElement is MCMElement ) {
                return CMType.MCM;
            } else if ( CMElement.SelectedCMElement is ACMElement ) {
                return CMType.ACM;
            }
            return CMType.NAN;
        }

        #endregion
    }
}