﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using CMtoOWL.Model.ConfainmentModel;
using CMtoOWL.UserControls.Widgets;

namespace CMtoOWL.Model.Project
{
    /// <summary>
    /// Реализует проект программы CMtoOWL
    /// </summary>
    internal class CMtoOWLProject
    {
        #region Свойства

        public string FileName {
            get;
            set;
        }

        public string Caption {
            get;
            set;
        }

        public CMList ProjectCMList {
            get;
            set;
        }

        public CMElementList ProjectCMElementList {
            get;
            set;
        }

        public CCM MainCCM {
            get;
            set;
        }

        public CMSelectionTab CMTab {
            get;
            set;
        }

        public CMTreeViewWidget CMTreeView {
            get;
            set;
        }

        public CMParametersWidget SelectedCMParameters {
            get;
            set;
        }

        public SelectedCMElementWidget SelectedCMElementParameters {
            get;
            set;
        }

        #endregion

        #region Методы

        public void ClearProject() {
            this.MainCCM = null;
            this.CMTab.ClearTabs();
            this.CMTreeView.ClearTreeViewItems();
            this.ProjectCMList = new CMList();
            this.ProjectCMElementList = new CMElementList();
        }

        /// <summary>
        /// Загружает проект из файла.
        /// </summary>
        /// <param name="filename">Путь к файлу.</param>
        public void OpenProjectFromFile( string filename ) {
            this.ClearProject();
            FileStream stream = new FileStream( filename, FileMode.Open );
            StreamReader string_reader = new StreamReader( stream );
            string input = string_reader.ReadToEnd();
            string_reader.Close();
            stream.Close();
            List<string> input_list = input.Split( new string[] { "\r\n" }, StringSplitOptions.None ).ToList();
            string hash = input_list[0];
            input_list.RemoveAt( 0 ); input_list.RemoveAt( 0 );
            hash = Serialize.GetSerializeStringParam( hash, "Hash : " );
            // проверяем целостность файла
            if ( MD5Hash.GetHashString( String.Join( "\r\n", input_list ) ) == hash ) {
                input_list.RemoveAt( 0 );
                string cm_caption = Serialize.GetSerializeStringParam( input_list[0], "Caption : " );
                input_list.RemoveAt( 0 );
                this.MainCCM = this.CMTab.AddNewCM( CMType.CCM, null, cm_caption ) as CCM;
                while ( input_list[0] != "" ) { 
                    int number = Serialize.GetSerializeIntParam( input_list[0], "Element Number : ");
                    input_list.RemoveAt( 0 ); 
                    this.MainCCM.CMElements[number].Text = Serialize.GetSerializeStringParam( input_list[0], "Element Text : " );
                    input_list.RemoveAt( 0 );
                }
                input_list.RemoveAt( 0 );
                while ( input_list.Count > 1 ) {
                    string cm_type = Serialize.GetSerializeStringParam( input_list[0], "CM : " );
                    input_list.RemoveAt( 0 );
                    cm_caption = Serialize.GetSerializeStringParam( input_list[0], "Caption : " );
                    input_list.RemoveAt( 0 );
                    int number = Serialize.GetSerializeIntParam( input_list[0], "Element Number : " );
                    input_list.RemoveAt( 0 );
                    string cm_element_text = Serialize.GetSerializeStringParam( input_list[0], "Element Text : " );
                    input_list.RemoveAt( 0 );
                    CMElement cm_element = this.ProjectCMElementList.Find( cm_element_text );
                    CM cm = null;
                    switch ( cm_type ) {
                        case "CCM":
                            cm = this.CMTab.AddNewCM( CMType.CCM, cm_element, cm_caption );
                            break;
                        case "MCM":
                            cm = this.CMTab.AddNewCM( CMType.MCM, cm_element, cm_caption );
                            break;
                        case "GCM":
                            cm = this.CMTab.AddNewCM( CMType.GCM, cm_element, cm_caption );
                            break;
                        case "ACM":
                            cm = this.CMTab.AddNewCM( CMType.ACM, cm_element, cm_caption );
                            string acm_type = Serialize.GetSerializeStringParam( input_list[0], "Type : " );
                            input_list.RemoveAt( 0 );
                            ( cm.CMElements[number] as ACMElement ).SetType( acm_type );
                            break;
                    }
                    while ( input_list[0] != "" ) {
                        number = Serialize.GetSerializeIntParam( input_list[0], "Element Number : " );
                        input_list.RemoveAt( 0 );
                        cm.CMElements[number].Text = Serialize.GetSerializeStringParam( input_list[0], "Element Text : " );
                        input_list.RemoveAt( 0 );
                        switch ( cm_type ) {
                            case "GCM":
                                while ( !input_list[0].StartsWith( "Element Number : " ) && input_list[0] != "" ) {
                                    string attr_name = Serialize.GetSerializeStringParam( input_list[0], "Attribute Name : " );
                                    input_list.RemoveAt( 0 );
                                    ACMElement acm_element = this.ProjectCMElementList.Find( attr_name ) as ACMElement;
                                    ( cm.CMElements[number] as GCMElement ).SetValue( 
                                        acm_element, 
                                        Serialize.GetSerializeStringParam( input_list[0], "Attribute Value : " ) 
                                    );
                                    input_list.RemoveAt( 0 );
                                }
                                break;
                            case "ACM":
                                string acm_type = Serialize.GetSerializeStringParam( input_list[0], "Type : " );
                                input_list.RemoveAt( 0 );
                                ( cm.CMElements[number] as ACMElement ).SetType( acm_type );
                                break;
                        }
                    }
                    input_list.RemoveAt( 0 );
                }
                this.CMTreeView.UpdateCMTreeView( this.MainCCM );
            } else {
                MessageBox.Show( "Входной файл был поврежден или содержит ошибку." );
            }
        }

        /// <summary>
        /// Сохраняет проект в файл.
        /// </summary>
        /// <param name="filename">Путь к файлу.</param>
        public void SaveProjectToFile( string filename ) {
            FileStream stream = new FileStream( filename, FileMode.Create );
            StreamWriter string_writer = new StreamWriter( stream );
            string output = this.MainCCM.SerializeData();
            output = "Hash : " + MD5Hash.GetHashString( output ) + "\r\n\r\n" + output;
            string_writer.Write( output );
            string_writer.Close();
            stream.Close();
            MessageBox.Show( "Проект сохранен" );
            this.FileName = filename;
        }

        #endregion

        #region Конструкторы

        public CMtoOWLProject() {
            this.FileName = "";
            this.ProjectCMList = new CMList();
            this.ProjectCMElementList = new CMElementList();
        }

        #endregion
    }
}
