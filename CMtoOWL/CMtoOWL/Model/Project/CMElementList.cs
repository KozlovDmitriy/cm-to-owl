﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.Model.Project
{   
    /// <summary>
    /// Реализует список элементов КМ
    /// </summary>
    public class CMElementList
    {
        #region Поля

        public List<CMElement> cm_element_list = new List<CMElement>();

        #endregion

        #region Свойства

        public List<CMElement> CMs {
            get {
                return this.cm_element_list;
            }
        }

        #endregion

        #region Методы

        public void Add( CMElement element ) {
            this.cm_element_list.Add( element );
        }

        public CMElement Find( string text ) {
            return this.cm_element_list.Find( x => x.Text == text );
        }

        public List<CMElement> FindAll( string text ) {
            return this.cm_element_list.FindAll( x => x.Text == text );
        }

        public int FindIndex( string text ) {
            return this.cm_element_list.FindIndex( x => x.Text == text );
        }

        public void Remove( CMElement element ) {
            if ( this.cm_element_list.Contains( element ) ) {
                this.cm_element_list.Remove( element );
            }
        }

        public void Remove( string text ) {
            this.cm_element_list.RemoveAll( x => x.Text == text );
        }

        #endregion
    }
}
