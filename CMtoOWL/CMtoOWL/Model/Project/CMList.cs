﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.Model.Project
{
    /// <summary>
    /// Реализует список КМ
    /// </summary>
    internal class CMList
    {
        #region Поля

        public List<CM> cm_list = new List<CM>();

        #endregion

        #region Свойства

        public List<CM> CMs {
            get {
                return this.cm_list;
            }
        }

        #endregion

        #region Методы

        public void Add( CM cm ) {
            this.cm_list.Add( cm );
        }

        public CM Find( string caption ) {
            return this.cm_list.Find( x => x.Caption == caption );
        }

        public int FindIndex( string caption ) {
            return this.cm_list.FindIndex( x => x.Caption == caption );
        }

        public void Remove( CM cm ) {
            this.cm_list.Remove( cm );
        }

        public void Remove( string caption ) {
            this.cm_list.RemoveAll( x => x.Caption == caption );
        }

        #endregion
    }
}
