﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Model.ConfainmentModel;
using CMtoOWL.Model.MathModel;

namespace CMtoOWL.Model.OwlTree
{
    public abstract class OwlTreeNode
    {
        public String Name {
            get;
            set;
        }

        public OwlClassNode Parent {
            get;
            set;
        }

        public List<OwlTreeNodeAttribute> Attributes {
            get;
            private set;
        }

        protected CMElement cm_element;

        public OwlTreeNode() {
            this.Attributes = new List<OwlTreeNodeAttribute>();
        }

        public string GetAttributeDeclarations() {
            string ret = "";
            foreach ( OwlTreeNodeAttribute attr in this.Attributes ) {
                ret += OwlScript.CreateDataPropertyDeclaration( attr.Name );
            }
            return ret;
        }

        public abstract string GetNamedDeclarations();

        public abstract string GetNodeRelationDeclarations();

        public abstract string GetIndividualsAttributes();
    }
}
