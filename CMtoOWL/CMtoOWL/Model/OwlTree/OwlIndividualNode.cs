﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Model.ConfainmentModel;
using CMtoOWL.Model.MathModel;
using CMtoOWL.Enums;

namespace CMtoOWL.Model.OwlTree
{
    /// <summary>
    /// Реализует экземпляр класса онтологии.
    /// </summary>
    public class OwlIndividualNode : OwlTreeNode
    {
        public OwlIndividualNode( string name, CMElement cm_element, OwlClassNode parent ) {
            this.Name = name;
            this.cm_element = cm_element;
            this.Parent = parent;
            parent.Childrens.Add( this );
        }

        public override string GetNamedDeclarations() {
            string ret = OwlScript.CreateNamedIndividualDeclaration( this.Name );
            ret += this.GetAttributeDeclarations();
            return ret;
        }

        public override string GetNodeRelationDeclarations() {
            return OwlScript.CreateClassAssertion( this.Parent.Name, this.Name );
        }

        public override string GetIndividualsAttributes() {
            GCMElement gcm_element = this.cm_element as GCMElement;
            gcm_element.CleanAttributeWithValues();
            string ret = "";
            foreach ( ACMElement attr in gcm_element.AttributeWithValues.Keys ) { 
                ret += OwlScript.CreateDataPropertyAssertion( attr.Text, this.Name, attr.Type.ToDescription(), gcm_element.GetValue( attr ).ToString() );
            }
            return ret;
        }
    }
}
