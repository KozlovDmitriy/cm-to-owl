﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Enums;
using CMtoOWL.Model.ConfainmentModel;

namespace CMtoOWL.Model.OwlTree
{
    public class OwlTreeNodeAttribute
    {
        public string Name {
            get;
            set;
        }

        public AttributeType Type {
            get;
            set;
        }

        public bool Compare( OwlTreeNodeAttribute other ) {
            return this.Name == other.Name;
        }

        private ACMElement acm_element;

        public OwlTreeNodeAttribute( string name, AttributeType type, ACMElement acm_element, OwlTreeNode attribute_node ) {
            this.Name = name;
            this.Type = type;
            this.acm_element = acm_element;
            attribute_node.Attributes.Add( this );
        }
    }
}
