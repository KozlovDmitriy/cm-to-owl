﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CMtoOWL.Model.ConfainmentModel;
using CMtoOWL.Model.MathModel;

namespace CMtoOWL.Model.OwlTree
{
    public class OwlClassNode : OwlTreeNode
    {
        public List<OwlTreeNode> Childrens {
            get;
            set;
        }

        public OwlClassNode( string name, CMElement cm_element, OwlClassNode parent ) {
            this.Childrens = new List<OwlTreeNode>();
            this.Name = name;
            this.cm_element = cm_element;
            this.Parent = parent;
            if ( parent != null ) {
                parent.Childrens.Add( this );
            }
        }

        public override string GetNamedDeclarations() {
            string ret = OwlScript.CreateClassDeclaration( this.Name );
            ret += this.GetAttributeDeclarations();
            foreach ( OwlTreeNode node in this.Childrens ) {
                ret += node.GetNamedDeclarations();
            }
            return ret;
        }

        public override string GetNodeRelationDeclarations() {
            string ret = "";
            if ( this.Parent != null && this.Parent.Name != "Thing" ) {
                ret += OwlScript.CreateSubclass( this.Name, this.Parent.Name );
            }
            foreach ( OwlTreeNode node in this.Childrens ) {
                ret += node.GetNodeRelationDeclarations();
            }
            return ret;
        }

        public override string GetIndividualsAttributes() {
            string ret = "";
            foreach ( OwlTreeNode node in this.Childrens ) {
                ret += node.GetIndividualsAttributes();
            }
            return ret;
        }
    }
}
