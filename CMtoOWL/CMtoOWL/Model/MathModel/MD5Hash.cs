﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace CMtoOWL
{
    /// <summary>
    /// Реализует класс вычисления MD5 хэш-функции.
    /// </summary>
    public class MD5Hash
    {
        #region Статические методы

        /// <summary>
        /// Вычисляет MD5 из строки.
        /// Необходимо для проверки целостности файла.
        /// </summary>
        /// <param name="sender">Строка.</param>
        static public string GetHashString( string s ) {
            //переводим строку в байт-массив 
            byte[] bytes = Encoding.Unicode.GetBytes( s );
            //создаем объект для получения средст шифрования  
            MD5CryptoServiceProvider CSP = new MD5CryptoServiceProvider();
            //вычисляем хеш-представление в байтах  
            byte[] byteHash = CSP.ComputeHash( bytes );
            string hash = string.Empty;
            //формируем одну цельную строку из массива  
            foreach ( byte b in byteHash )
                hash += string.Format( "{0:x2}", b );
            return hash;
        }

        #endregion
    }
}
