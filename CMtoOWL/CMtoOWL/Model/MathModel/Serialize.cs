﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMtoOWL
{
    /// <summary>
    /// Реализует методы сериализации проекта.
    /// </summary>
    public class Serialize
    {
        #region Статические методы

        /// <summary>
        /// Получает из строки переменную типа double.
        /// </summary>
        /// <param name="str">Строка.</param>
        /// <param name="start_str">Префикс строки.</param>
        /// <returns></returns>
        public static double GetSerializeDoubleParam( string str, string start_str ) {
            return str.StartsWith( start_str ) ? Convert.ToDouble( str.Remove( 0, start_str.Length ) ) : 0.0;
        }

        /// <summary>
        /// Получает из строки переменную типа string.
        /// </summary>
        /// <param name="str">Строка.</param>
        /// <param name="start_str">Префикс строки.</param>
        /// <returns></returns>
        public static string GetSerializeStringParam( string str, string start_str ) {
            return str.StartsWith( start_str ) ? str.Remove( 0, start_str.Length ) : "";
        }

        /// <summary>
        /// Получает из строки переменную типа uint.
        /// </summary>
        /// <param name="str">Строка.</param>
        /// <param name="start_str">Префикс строки.</param>
        /// <returns></returns>
        public static uint GetSerializeUintParam( string str, string start_str ) {
            return str.StartsWith( start_str ) ? Convert.ToUInt32( str.Remove( 0, start_str.Length ) ) : 0;
        }
        
        /// <summary>
        /// Получает из строки переменную типа int.
        /// </summary>
        /// <param name="str">Строка.</param>
        /// <param name="start_str">Префикс строки.</param>
        /// <returns></returns>
        public static int GetSerializeIntParam( string str, string start_str ) {
            return str.StartsWith( start_str ) ? Convert.ToInt32( str.Remove( 0, start_str.Length ) ) : 0;
        }

        /// <summary>
        /// Получает из строки переменную типа bool.
        /// </summary>
        /// <param name="str">Строка.</param>
        /// <param name="start_str">Префикс строки.</param>
        /// <returns></returns>
        public static bool GetSerializeBoolParam( string str, string start_str ) {
            return str.StartsWith( start_str ) ? Convert.ToBoolean( str.Remove( 0, start_str.Length ) ) : false;
        }

        #endregion
    }
}
