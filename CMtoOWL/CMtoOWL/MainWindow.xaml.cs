﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CMtoOWL.Enums;
using CMtoOWL.Model.ConfainmentModel;
using CMtoOWL.Model.Project;
using CMtoOWL.UserControls;
using CMtoOWL.UserControls.Widgets;
using CMtoOWL.Windows;
using Microsoft.Win32;

namespace CMtoOWL
{
    /// <summary>
    /// Класс главного окна.
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Поля

        CMtoOWLProject current_project = new CMtoOWLProject();
        /// <summary>
        /// Диалоговое окно сохранения файла.
        /// </summary>
        private SaveFileDialog save_file_dialog;
        /// <summary>
        /// Диалоговое окно открытия файла.
        /// </summary>
        private OpenFileDialog open_file_dialog;

        #endregion

        #region Конструкторы

        public MainWindow() {
            InitializeComponent();
            CMElement.CMElementTextChanged += CMElement_CMElementTextChanged;
            CMElement.CMElementSelectChanged += CMElement_CMElementSelectChanged;
            this.save_file_dialog = new SaveFileDialog();
            this.open_file_dialog = new OpenFileDialog();
        }

        private void MainWindow_Loaded( object sender, RoutedEventArgs e ) {
            this.current_project.CMTab = this.tc_cm_tab_control;
            this.current_project.CMTab.CMSelectionTabSelectionChanged += CMSelectionTab_CMSelectionTabSelectionChanged;
            this.current_project.CMTab.AddNewCMToCMSelectionTab += CMSelectionTab_AddNewCMToCMSelectionTab;
            this.current_project.CMTreeView = this.cm_tree;
            this.current_project.CMTreeView.CMTreeViewSelectionChanged += SelectionCMChanged;
            this.current_project.SelectedCMParameters = this.cm_parameters;
            this.current_project.SelectedCMParameters.CMElementSelectionChanged += CMParametersWidget_CMElementSelectionChanged;
            this.current_project.SelectedCMParameters.CMCaptionChanged += CMParametersWidget_CMCaptionChanged;
            this.current_project.SelectedCMElementParameters = this.selected_cm_element;
            this.current_project.SelectedCMElementParameters.SelectedCMElementNameChanged += 
                SelectedCMElementParameters_SelectedCMElementNameChanged;
            this.current_project.SelectedCMElementParameters.SelectionCMChanged += SelectionCMChanged;
            this.current_project.MainCCM = this.current_project.CMTab.AddNewCM( CMType.CCM, CMElement.SelectedCMElement, "Уровень 1" ) as CCM;
            this.current_project.CMTreeView.UpdateCMTreeView( this.current_project.MainCCM );
        }

        #endregion
        
        #region Функции-члены

        private CM GetCurrentCm() {
            return this.current_project.ProjectCMList.Find( this.current_project.CMTab.SelectionCMHeader );
        }

        #endregion

        #region Команды
        
        /// <summary>
        /// Выполняет команду создания нового файла с диаграммой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void NewProject( object sender, ExecutedRoutedEventArgs e ) {
            MessageBoxResult message_box_result = MessageBox.Show(
                    "Вы действительно хотите перейти к созданию нового проекта?\nВСЕ НЕСОХРАНЕННЫЕ ДАННЫЕ БУДУТ УТЕРЯНЫ!",
                    "Подтверждение создания нового проекта",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question
                );
            if ( message_box_result == MessageBoxResult.Yes ) {
                this.current_project.ClearProject();
                this.current_project.MainCCM = this.current_project.CMTab.AddNewCM( CMType.CCM, CMElement.SelectedCMElement, "Уровень 1" ) as CCM;
                this.current_project.CMTreeView.UpdateCMTreeView( this.current_project.MainCCM );
            }
        }

        /// <summary>
        /// Выполняет команду сохранения файла с диаграммой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void SaveAs( object sender, ExecutedRoutedEventArgs e ) {
            this.save_file_dialog.Title = "Сохранить как";
            this.save_file_dialog.Filter = "CTO File|*.cto";
            if ( this.save_file_dialog.ShowDialog() == true ) {
                this.current_project.SaveProjectToFile( this.save_file_dialog.FileName );
            }
        }

        /// <summary>
        /// Выполняет команду сохранения файла с диаграммой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Save( object sender, ExecutedRoutedEventArgs e ) {
            if ( this.current_project.FileName.Length == 0 ) {
                SaveAs( sender, e );
            } else {
                this.current_project.SaveProjectToFile( this.current_project.FileName );
            }
        }

        /// <summary>
        /// Выполняет команду открытия файла с диаграммой.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Open( object sender, ExecutedRoutedEventArgs e ) {
            e.Handled = true;
            MessageBoxResult message_box_result = MessageBox.Show(
                    "Вы действительно хотите открыть другой проект?\nВСЕ НЕСОХРАНЕННЫЕ ДАННЫЕ БУДУТ УТЕРЯНЫ!",
                    "Подтверждение открытия проекта",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Question
                );
            if ( message_box_result == MessageBoxResult.Yes ) {
                this.open_file_dialog.Title = "Открыть проект";
                this.open_file_dialog.Filter = "CTO File|*.cto";
                if ( this.open_file_dialog.ShowDialog() == true ) {
                    this.current_project.FileName = this.open_file_dialog.FileName;
                    this.current_project.OpenProjectFromFile( this.open_file_dialog.FileName );
                }
            }
        }

        /// <summary>
        /// Выполняет команду выхода из программы.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Exit( object sender, ExecutedRoutedEventArgs e ) {
            this.Close();
        }

        /// <summary>
        /// Проверка на доступность команд.
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void CommandCanExecutive( object sender, CanExecuteRoutedEventArgs e ) {
            e.CanExecute = true;
        }

        private void ClearKeyboardFocusCanExecute( object sender, CanExecuteRoutedEventArgs e ) {
            e.CanExecute = true;
            e.Handled = true;
        }

        private void ClearKeyboardFocusExecuted( object sender, ExecutedRoutedEventArgs e ) {
            FocusManager.SetFocusedElement( this, this );
            Keyboard.ClearFocus();
        }

        private void CreateNewLevelCanExecute( object sender, CanExecuteRoutedEventArgs e ) {
            bool can_execute = false;
            if ( CMElement.SelectedCMElement != null && 
                CMElement.SelectedCMElement.State == CMElementState.FULL && 
                !(CMElement.SelectedCMElement is ACMElement ) ) {
                can_execute = true;
            }
            e.CanExecute = can_execute;
            e.Handled = true;
        }

        private void CreateNewLevelExecuted( object sender, ExecutedRoutedEventArgs e ) {
            CMType type = CMElement.GetCurrentCMType();
            if ( type == CMType.CCM || type == CMType.GCM || type == CMType.MCM ) {
                ChoiceNextCMWindow window = null;
                if ( type == CMType.CCM && CMElement.SelectedCMElement.ElementNumber == 0 ) {
                    window = new ChoiceNextCMWindow( type, true );
                } else {
                    window = new ChoiceNextCMWindow( type, false );
                }
                if ( window.ShowDialog() == true ) {
                    if ( window.Caption != "" && this.current_project.ProjectCMList.Find( window.Caption ) == null ) {
                        this.current_project.CMTab.AddNewCM( window.ChoiceCMType, CMElement.SelectedCMElement, window.Caption );
                        this.current_project.CMTreeView.UpdateCMTreeView( this.current_project.MainCCM );
                        this.current_project.SelectedCMElementParameters.UpdateCMElementWidgetInfo( CMElement.SelectedCMElement );
                    } else {
                        MessageBox.Show( "КМ с таким именем уже существует." );
                    }
                }
            }
        }

        #endregion

        #region События

        private void btn_to_owl_Click( object sender, RoutedEventArgs e ) {
            this.save_file_dialog.Title = "Экспорт в OWL";
            this.save_file_dialog.Filter = "OWL File|*.owl";
            if ( this.save_file_dialog.ShowDialog() == true ) {
                FileStream stream = new FileStream( this.save_file_dialog.FileName, FileMode.Create );
                StreamWriter string_writer = new StreamWriter( stream );
                string output = this.current_project.MainCCM.ToOwl();
                string_writer.Write( output );
                string_writer.Close();
                stream.Close();
                MessageBox.Show( "Экспорт проекта в OWL завершен." );
            }
        }

        /// <summary>
        /// Выполняет операцию экспорта текущего уровня КМ в картинку.
        /// Событие Click на пункте меню "Экспорт".
        /// </summary>
        /// <param name="sender">Источник.</param>
        /// <param name="e">Параметры.</param>
        private void Export( object sender, RoutedEventArgs e ) {
            this.save_file_dialog.Title = "Сохранить как изображение";
            this.save_file_dialog.Filter = "PNG Image|*.png|JPEG Image|*.jpg|BMP Image|*.bmp|GIF Image|*.gif|TIFF Image|*.tif";
            if ( this.save_file_dialog.ShowDialog() == true ) {
                try {
                    this.GetCurrentCm().View.ExportCanvasToImage( this.save_file_dialog.FileName );
                    MessageBox.Show( "Экспорт завершен." );
                }
                catch ( System.Exception ex ) {
                    MessageBox.Show( ex.Message );
                }
            }
        }

        /// <summary>
        /// Действие по закрытию окна
        /// Сохраняем пользовательские настройки
        /// </summary>
        /// <param name="e">Параметры.</param>
        private void Window_Closing( object sender, System.ComponentModel.CancelEventArgs e ) {
            MessageBoxResult message_box_result = MessageBox.Show(
                "Вы выходите из программы!\n" +
                "Хотите сохранить ваш текущий проект?",
                "Приложение будет закрыто",
                MessageBoxButton.YesNoCancel,
                MessageBoxImage.Question );
            if ( message_box_result == MessageBoxResult.Cancel ) {
                e.Cancel = true;
                return;
            } else if ( message_box_result == MessageBoxResult.Yes ) {
                Save( this, null );
            }

        }

        private void CMSelectionTab_AddNewCMToCMSelectionTab( CM cm ) {
            if ( cm != null ) {
                this.current_project.ProjectCMList.Add( cm );
            }
        }

        private void CMParametersWidget_CMCaptionChanged( string caption ) {
            if ( caption != this.GetCurrentCm().Caption ) {
                if ( caption != "" && this.current_project.ProjectCMList.Find( caption ) == null ) {
                    this.GetCurrentCm().Caption = caption;
                    this.current_project.CMTab.SelectionCMHeader = caption;
                    this.current_project.CMTreeView.UpdateCMTreeView( this.current_project.MainCCM );
                } else {
                    MessageBox.Show( "КМ с таким именем уже существует." );
                    this.current_project.SelectedCMParameters.CMCaption = this.GetCurrentCm().Caption;
                }
            }
        }

        private void CMParametersWidget_CMElementSelectionChanged( string text ) {
            if ( CMElement.SelectedCMElement == null || text != CMElement.SelectedCMElement.Text ) {
                CM cm = this.GetCurrentCm();
                if ( cm != null ) {
                    for ( int i = 0; i < 9; ++i ) {
                        if ( cm.CMElements[i] != null ) {
                            if ( cm.CMElements[i].State == CMElementState.FULL || cm.CMElements[i].State == CMElementState.FULL_FIXED ) {
                                if ( cm.CMElements[i].Text == text ) {
                                    cm.CMElements[i].Select();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void CMElement_CMElementSelectChanged() {
            CMElement element = CMElement.SelectedCMElement;
            if ( element != null ) {
                //Выделить элемент в списке элементов
                this.current_project.SelectedCMParameters.SelectedCMElementText = element.Text;
            } else {
                this.current_project.SelectedCMParameters.SelectedCMElementIndex = -1;
            }
            this.current_project.SelectedCMElementParameters.UpdateCMElementWidgetInfo( element );
        }

        private void SelectionCMChanged( string header ) {
            if ( this.current_project.CMTab.SelectionCMHeader != header ) {
                this.current_project.CMTab.SelectCM( header );
            }
        }

        private void CMSelectionTab_CMSelectionTabSelectionChanged( string cm_caption ) {
            this.current_project.CMTreeView.RemoveSelections();
            this.current_project.SelectedCMParameters.CMCaption = cm_caption;
            this.current_project.SelectedCMParameters.UpdateCurrentCMElements( this.GetCurrentCm() );
        }

        private void CMElement_CMElementTextChanged() {
            CMElement element = CMElement.SelectedCMElement;
            if ( element.CM != null ) {
                List<CMElement> list = this.current_project.ProjectCMElementList.FindAll( element.Text );
                if ( list.Count == 0 || ( list.Count == 1 && list[0] == element ) ) {
                    for ( int i = 0; i < 9; ++i ) {
                        this.current_project.ProjectCMElementList.Remove( element.CM.CMElements[i] );
                        if ( element.CM.CMElements[i].State == CMElementState.FULL ) {
                            this.current_project.ProjectCMElementList.Add( element.CM.CMElements[i] );
                        }
                    }
                    this.current_project.SelectedCMElementParameters.UpdateCMElementWidgetInfo( element );
                    this.current_project.SelectedCMParameters.UpdateCurrentCMElements( this.GetCurrentCm() );
                } else {
                    MessageBox.Show( "Элемент с таким именем уже существует" );
                    element.Text = element.OldText;
                    element.UpdateCmElementsState();
                }
            }
        }

        private void SelectedCMElementParameters_SelectedCMElementNameChanged( string name ) {
            CMElement element = CMElement.SelectedCMElement;
            if ( element != null ) {
                string text = this.current_project.SelectedCMElementParameters.CMElementName;
                List<CMElement> list = this.current_project.ProjectCMElementList.FindAll( text );
                if ( list.Count == 0 || ( list.Count == 1 && list[0] == element ) ) {
                    element.Text = text;
                    for ( int i = 0; i < 9; ++i ) {
                        this.current_project.ProjectCMElementList.Remove( element.CM.CMElements[i] );
                        if ( element.CM.CMElements[i].State == CMElementState.FULL ) {
                            this.current_project.ProjectCMElementList.Add( element.CM.CMElements[i] );
                        }
                    }
                    element.UpdateCmElementsState();
                    this.current_project.SelectedCMParameters.UpdateCurrentCMElements( this.GetCurrentCm() );
                }
                //this.current_project.SelectedCMElementParameters.CMElementName = element.Text;
                this.current_project.SelectedCMParameters.SelectedCMElementText = element.Text;
            }
        }

        #endregion
    }
}